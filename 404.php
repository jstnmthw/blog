<?php
/**
 * Home page template file
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage justin
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

	<main id="main" class="site-main home" role="main">
		<div class="dc">
			<h1>404</h1>
			<p>Oops, looks like we messed up. Page not found.</p>
			<p><a href="<?php echo site_url(); ?>">Back to the home page &rarr;</a></p>
		</div>
	</main>

<?php get_footer(); ?>