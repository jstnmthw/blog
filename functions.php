<?php

/**
 * Enqueue scripts and styles.
 */
function scripts() {

    // Theme stylesheet
    wp_enqueue_style( 'style', get_stylesheet_uri() );
    wp_enqueue_style( 'fonts', 'https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,700');
    wp_enqueue_style( 'icons', 'https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css');

    // Link Fix
    wp_enqueue_script( 'skip-link-focus-fix', get_theme_file_uri( '/assets/js/skip-link-focus-fix.js' ), array(), '1.0', true );

    // Update to latest jQuery & Plugins
    wp_deregister_script( 'jquery' );
    wp_enqueue_script( 'jquery', 'https://code.jquery.com/jquery-3.2.1.min.js', array(), null, true );

    // Bootstrap 4
    wp_enqueue_script( 'popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js', array('jquery'), null, true );
    wp_enqueue_script( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js', array('jquery'), null, true );

    // Our Scripts
    wp_enqueue_script( 'main', get_theme_file_uri( '/assets/js/main.js' ), array('jquery'), '1.0', true );

    // Defer JS
    // function add_async_js($tag, $handle) {
    //     if ( 'jquery' !== $handle )
    //         return $tag;
    //     return str_replace( ' src', ' async="async" src', $tag );
    // }
    // add_filter( 'script_loader_tag', 'add_async_js', 10, 2 );

    // Comment Reply
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }

}
add_action( 'wp_enqueue_scripts', 'scripts' );

/**
 * Front end filters
 */
add_filter( 'next_posts_link_attributes', 'next_posts_link_attributes' );
add_filter( 'previous_posts_link_attributes', 'prev_posts_link_attributes' );
function next_posts_link_attributes() {
    return 'class="next-btn"';
}
function prev_posts_link_attributes() {
    return 'class="prev-btn"';
}

/**
 * Remove read more anchor
 */
function remove_more_link_scroll( $link ) {
    $link = preg_replace( '|#more-[0-9]+|', '', $link );
    return $link;
}
add_filter( 'the_content_more_link', 'remove_more_link_scroll' );

/**
 * Prism.js - Syntax Highlighting
 */
function add_prism() {

    // Register prism.css file
    wp_register_style(
        'prismCSS',
        get_theme_file_uri() . '/assets/css/vendor/prism-ocean_theme.css'
    );

    // Register prism.js file
    wp_register_script(
        'prismJS',
        get_theme_file_uri() . '/assets/js/vendor/prism.js'
    );

    // Enqueue the registered style and script files
    wp_enqueue_style( 'prismCSS' );
    wp_enqueue_script( 'prismJS' );

}
add_action( 'wp_enqueue_scripts', 'add_prism' );

// Features Images (post thumbnails)
add_theme_support( 'post-thumbnails' );

// Remove WP Emoji
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );
