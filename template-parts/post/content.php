<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage justin
 * @since 1.0
 * @version 1.2
 */

?>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<?php
			if ( is_sticky() && is_home() ) :
				echo '<i title="Stickied" class="icon-pin sticky"></i>';
			endif;
			?>

			<div class="entry-date">
				<?php the_time('F jS, Y'); ?> in <?php the_category(', ') ?>.
			</div>

			<header class="entry-header">
				<?php
				if ( 'post' === get_post_type() ) {
					echo '<div class="entry-meta">';
						if ( is_single() ) {
						} else {
						};
					echo '</div>';
				};

				if ( is_single() ) {
					the_title( '<h1 class="entry-title">', '</h1>' );
				} elseif ( is_front_page() && is_home() ) {
					the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
				} else {
					the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
				}
				?>
			</header>

			<div class="entry-content">
				<?php
				/* translators: %s: Name of current post */
				the_content();

				wp_link_pages( array(
					'before'      => '<div class="page-links">' . __( 'Pages:', 'justin' ),
					'after'       => '</div>',
					'link_before' => '<span class="page-number">',
					'link_after'  => '</span>',
				) );
				?>
			</div>

			<!-- <div class="entry-footer">
				<i class="icon-bubbles"></i> <a href="<?php comments_link(); ?>"><?php comments_number( '0', '1'); ?> Comments</a>
			</div> -->

		</article>
