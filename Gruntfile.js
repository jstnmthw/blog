module.exports = function (grunt) {

	// All plugin configurations goes here
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		// SVG Icons
		svgstore: {
			options: {
				prefix: 'icon-',
				svg: {
					xmlns: 'http://www.w3.org/2000/svg'
				}
			},
			default: {
				files: { 'assets/icons/svg-icons.svg': ['src/svg-icons/*.svg'] }
			}
		},

		// Grunt SASS
		sass: {
			options: {
				sourceMap: true,
				outputStyle: 'compressed'
			},
			dist: {
				files: {
					'style.css': 'src/scss/style.scss',
					'assets/css/vendor/prism-ocean_theme.css': 'assets/css/vendor/prism-ocean_theme.scss'
				}
			}
		},

		// PostCSS
		postcss: {
			options: {
				map: true,
				processors: [
					require('autoprefixer')({
						browsers: [
							'last 2 versions',
							'Chrome >= 30',
							'Firefox >= 30',
							'ie >= 8',
							'Safari >= 8'
						]
					})
				]
			},
			dist: {
				src: 'style.css'
			}
		},

		// Watch SASS
		watch: {
			css: {
				files: 'src/scss/**/*.{scss,sass}',
				tasks: ['sass:dist', 'postcss']
			}
		},

		// Uglify JS
		// uglify: {
		// 	my_target: {
		// 		options : {
		// 			mangle: false,
		// 			compress: true,
		// 			preserveComments: false,
		// 			screwIE8: false,
		// 			banner: '/*!\r\n *  Scripts.js compiled on <%= grunt.template.today("mm-dd-yyyy") %> \r\n */\r\n'
		// 		},
		// 		files: {
		// 			'public_html/js/scripts.js':
		// 			[
		// 				'public_html/js/libs/jquery-3.1.1.min.js',
		// 				'public_html/js/plugins.js',
		// 				'public_html/js/main.js'
		// 			]
		// 		}
		// 	}
		// }

	});

	// Tell Grunt we want to use these plug-ins.
	grunt.loadNpmTasks('grunt-svgstore');
	grunt.loadNpmTasks('grunt-postcss');
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-uglify');

	// Tell Grunt what to do when we type "grunt" into the terminal.
	grunt.registerTask('prefix', ['postcss']);
	grunt.registerTask('svg', ['svgstore']);
	grunt.registerTask('minjs', ['uglify']);
	grunt.registerTask('default', ['watch']);
	grunt.registerTask('dist', ['sass', 'postcss']);

};