/*
 *
 *	Main.js
 *	Add initializers from plugins.js or global JS only.
 *
 */
$(function () {

	/* SVG Loader */
	$.get("/wp-content/themes/justin/assets/icons/svg-icons.svg", function (data) {
		var div = document.createElement("div");
		div.id = 'inline-svg';
		div.innerHTML = new XMLSerializer().serializeToString(data.documentElement);
		document.body.insertBefore(div, document.body.childNodes[0]);
	});

});