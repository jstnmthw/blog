<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage justin
 * @since 1.0
 * @version 1.2
 */

?>
<footer class="footer">
	<p>&copy; Copyright <?php echo date('Y'); ?> <a href="/" title="justin.ly">justin.ly</a>.<br>Powered by <i title="coffee" class="icon-cup"></i>.</p>
</footer>

<?php wp_footer(); ?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-16970942-2"></script>
<script nonce="gsdj6s8aLFWhnBx2JYrH0njq5Qk+zEV5IFxi1LfRIXU=">
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());
	gtag('config', 'UA-16970942-2');
</script>

</body>
</html>