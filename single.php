<?php
/**
 * Home page template file
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage justin
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
    
    
	<nav class="breadcrumbs">
		<ul>
			<li><a href="<?php echo get_home_url(); ?>">Home</a></li>
			<li>&raquo;</li>
			<li><?php the_category(', '); ?></li>
			<li>
		</ul>
	</nav>
	<main id="main" class="site-main" role="main">
		<?php
		if ( have_posts() ) :
			while ( have_posts() ) : the_post();
				get_template_part( 'template-parts/post/single', get_post_format() );
			endwhile;
		endif;
		?>
		<div class="about-author">
			<?php echo get_avatar( get_the_author_meta('email'), 60 ); ?> 
			<h4><?php echo get_the_author_meta('first_name') . ' ' . get_the_author_meta('last_name'); ?><a href="https://www.twitter.com/jstnly" class="follow-btn">Follow</a></h4>
			<p><?php echo get_the_author_meta('description'); ?></p>
		</div>
		<?php 
		// If comments are open or we have at least one comment, load up the comment template.
		if ( comments_open() || get_comments_number() ) :
			comments_template();
		endif;
		?>
	</main>

<?php get_footer(); ?>