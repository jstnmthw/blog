<?php
/**
 * Home page template file
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage justin
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

	<?php if(get_query_var('paged') == 0) : ?>
	<header class="masthead">
		<div class="intro">
			<img class="portrait" src="/wp-content/themes/justin/assets/imgs/portrait.jpg" alt="My Picture" title="My Picture">
			<h1 class="title">Justin.ly</h1>
			<h2 class="subtitle">Scribbles from a web developer.</h2>
			<ul class="social">
				<li>
					<a target="_blank" href="https://www.twitter.com/jstnly"><i class="icon-social-twitter"></i></a>
				</li>
				<li>
					<a target="_blank" href="https://www.linkedin.com/in/jstnmthw"><i class="icon-social-linkedin"></i></a>
				</li>
				<li>
					<a target="_blank" href="https://github.com/jstnmthw"><i class="icon-social-github"></i></a>
				</li>
			</ul>
		</div>
	</header>
	<?php endif; ?>
	<main id="main" class="site-main" role="main">
		<?php
		if ( have_posts() ) :
			while ( have_posts() ) : the_post();
				get_template_part( 'template-parts/post/content', get_post_format() );
			endwhile;
		?>
		<nav class="pagination row no-gutters">
			<div class="col-sm-12 col-md prev-col">
				<?php previous_posts_link( '&larr; Earlier Posts' ); ?>
			</div>
			<div class="col-sm-12 col-md pages">
				<?php 
					global $wp_query;
					echo 'Page '.max( 1, get_query_var('paged') ).' of '.$wp_query->max_num_pages;
				?>
			</div>
			<div class="col-sm-12 col-md next-col">
				<?php next_posts_link( 'Previous Posts &rarr;' ); ?>
			</div>
		</nav>
		<?php
		else :
			get_template_part( 'template-parts/post/content', 'none' );
		endif;
		?>
	</main>

<?php get_footer(); ?>