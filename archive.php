<?php
/**
 * Home page template file
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage justin
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

	<main id="main" class="site-main" role="main">
		<?php
		if ( have_posts() ) :
			while ( have_posts() ) : the_post();
				get_template_part( 'template-parts/post/content', get_post_format() );
			endwhile;
		?>
		<nav class="pagination row no-gutters">
			<div class="col prev-col"><?php previous_posts_link( '&larr; Previous Posts' ); ?></div>
			<div class="col pages">
			<?php 
				global $wp_query;
				echo 'Page '.max( 1, get_query_var('paged') ).' of '.$wp_query->max_num_pages;
			?>
			</div>
			<div class="col next-col"><?php next_posts_link( 'Next Posts &rarr;' ); ?></div>
		</nav>
		<?php
		else :
			get_template_part( 'template-parts/post/content', 'none' );
		endif;
		?>
	</main>

<?php get_footer(); ?>