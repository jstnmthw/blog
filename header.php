<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage justin
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php if(is_home()) : echo get_bloginfo('description') . ' - ' . get_bloginfo('name'); else: wp_title('-', true, 'right'); endif; ?></title>
	<meta name="google-site-verification" content="fZqzJN5-_GaIU8AVt__y96YO6VNmggOr_qk21QjPz7E" />
	
	<!-- Favico -->
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png?v=GvkW6paZe32">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png?v=GvkW6paZe32">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png?v=GvkW6paZe32">
	<link rel="manifest" href="/manifest.json?v=GvkW6paZe32">
	<link rel="mask-icon" href="/safari-pinned-tab.svg?v=GvkW6paZe32" color="#5bbad5">
	<link rel="shortcut icon" href="/favicon.ico?v=GvkW6paZe32">
	<meta name="theme-color" content="#1b2b34">

	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<a class="skip-link sr-only" href="#main"><?php _e( 'Skip to content', 'justin' ); ?></a>
